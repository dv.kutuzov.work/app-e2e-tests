import { Page } from '@playwright/test';
import { User, appUrl } from '@e2e-tests/utils';

// api example
export class ServerApi {
  protected page: Page;

  constructor(page: Page) {
    this.page = page;
  }

  public async login(user: User) {
    const { email, password } = user;

    const data = {
      email,
      password,
    };

    const response = await this.page.request.post(`${appUrl}/auth/login`, {
      data,
    });

    if (!response.ok()) {
      throw new Error(
        `HTTP error: ${response.status()}, message: ${await response.text()}`,
      );
    }

    return response.ok();
  }

  public async logout() {
    const response = await this.page.request.delete(`${appUrl}/auth/logout`);

    if (!response.ok()) {
      throw new Error(
        `HTTP error: ${response.status()}, message: ${await response.text()}`,
      );
    }

    return response.ok();
  }
}
