import { test } from '@e2e-tests/main-fixtures';

test.describe('video', () => {
  test.beforeEach(async ({ app, skipDicslaimer }) => {
    await test.step('open main page', async () => {
      await app.goToApp();

      await skipDicslaimer();
    });
  });

  test('open video from new creators slider', async ({ main, videoPlayer }) => {
    await test.step('open random video', async () => {
      await main.byNewCreatorsSlider.clickRandomVideo();

      await videoPlayer.toBeVisible();
    });

    await test.step('check video', async () => {
      await videoPlayer.videoPurchaseSection.toBeHidden(10000);

      await videoPlayer.checkMuted();

      await videoPlayer.checkAutoplay();
    });
  });
});
