import { test as base } from '@e2e-tests/component-fixtures';
import { MainPage } from '@e2e-tests/main-pages';
import { DisclaimerPage } from '@e2e-tests/disclaimer-pages';
import { VideoPlayerPage } from '@e2e-tests/video-player-pages';

interface IMainFixture {
  main: MainPage;
  disclaimer: DisclaimerPage;
  videoPlayer: VideoPlayerPage;
  skipDicslaimer: () => Promise<void>;
}

export const test = base.extend<IMainFixture>({
  main: async ({ page }, use) => {
    const main = MainPage.init({ page });

    await use(main);
  },

  disclaimer: async ({ page }, use) => {
    const disclaimer = DisclaimerPage.init({ page });

    await use(disclaimer);
  },

  videoPlayer: async ({ page }, use) => {
    const videoPlayer = VideoPlayerPage.init({ page });

    await use(videoPlayer);
  },

  skipDicslaimer: async ({ disclaimer }, use) => {
    const skipDicslaimer = async () => {
      await disclaimer.yesButton.click();

      await disclaimer.toBeHidden();
    };

    await use(skipDicslaimer);
  },
});
