import { test as base } from '@playwright/test';
import { User, appUrl } from '@e2e-tests/utils';
import { ServerApi } from 'tools/_tools/src';

interface ITestFactory {
  app: {
    user: User;
    goToApp: (subUrl?: string) => Promise<void>;
    signIn: (user: User) => Promise<void>;
    logout: () => Promise<void>;
    closeTab: (tab: number) => Promise<void>;
    clearCookie: () => Promise<void>;
    clearLocalStorage: () => Promise<void>;
    clearSessionStorage: () => Promise<void>;
  };
  api: ServerApi;
}

export const test = base.extend<ITestFactory>({
  app: async ({ page, context, api }, use) => {
    const user = User.init();

    const goToApp = async (subUrl?: string) => {
      const url = subUrl ? `${appUrl}${subUrl}` : `${appUrl}`;

      await page.goto(url);
    };

    const signIn = async (user: User) => {
      await api.login(user);

      await goToApp();
    };

    const logout = async () => {
      await api.logout();
    };

    const closeTab = async (tab: number) => {
      const allPages = context.pages();
      const tabToClose = allPages[tab - 1];
      await tabToClose?.close();
    };

    const clearCookie = async () => {
      await context.clearCookies();
    };

    const clearLocalStorage = async () => {
      await page.evaluate(() => localStorage.clear());
    };

    const clearSessionStorage = async () => {
      await page.evaluate(() => sessionStorage.clear());
    };

    await use({
      user,
      goToApp,
      signIn,
      logout,
      closeTab,
      clearCookie,
      clearLocalStorage,
      clearSessionStorage,
    });
  },

  api: async ({ page }, use) => {
    const api = new ServerApi(page);

    await use(api);
  },
});
