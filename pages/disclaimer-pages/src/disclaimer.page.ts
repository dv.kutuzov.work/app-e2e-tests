import { ComponentPage, IComponentPageProps } from '@e2e-tests/components';

export class DisclaimerPage extends ComponentPage {
  public warning: ComponentPage;

  public contentInterestTitle: ComponentPage;

  public straightButton: ComponentPage;

  public gayButton: ComponentPage;

  public transgenderButton: ComponentPage;

  public yesButton: ComponentPage;

  constructor(props: IComponentPageProps) {
    super(props);

    this.warning = new ComponentPage({
      page: this.page,
      locator: this.locator.locator('h2'),
    });

    this.contentInterestTitle = new ComponentPage({
      page: this.page,
      locator: this.locator.locator('h3'),
    });

    this.straightButton = new ComponentPage({
      page: this.page,
      locator: this.locator.locator('button[data-value="straight"]'),
    });

    this.gayButton = new ComponentPage({
      page: this.page,
      locator: this.locator.locator('button[data-value="gay"]'),
    });

    this.transgenderButton = new ComponentPage({
      page: this.page,
      locator: this.locator.locator('button[data-value="shemale"]'),
    });

    this.yesButton = new ComponentPage({
      page: this.page,
      locator: this.locator.locator('button[id="yes-im-over-18"]'),
    });
  }

  public static init(props: Omit<IComponentPageProps, 'locator'>) {
    return new DisclaimerPage({
      ...props,
      locator: props.page.locator('[data-el="VisitorAgreement"]'),
    });
  }
}
