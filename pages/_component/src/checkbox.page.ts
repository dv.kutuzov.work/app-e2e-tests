import { ComponentPage } from './component.page';
import { test } from '@playwright/test';

export class CheckboxPage extends ComponentPage {
  public async toBeChecked() {
    await test.expect(this.locator).toBeChecked();
  }
}
