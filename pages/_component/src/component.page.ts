import { Locator, Page } from '@playwright/test';
import { test } from '@playwright/test';

export interface IComponentPageProps {
  page: Page;
  locator: Locator;
}

export class ComponentPage {
  protected page: Page;

  protected locator: Locator;

  constructor(props: IComponentPageProps) {
    this.page = props.page;
    this.locator = props.locator;
  }

  public async click(options?: Parameters<Locator['click']>[0]) {
    await this.locator.click(options);
  }

  public async dblclick(options?: Parameters<Locator['dblclick']>[0]) {
    await this.locator.dblclick(options);
  }

  public async hover(options?: Parameters<Locator['hover']>[0]) {
    await this.locator.hover(options);
  }

  public nth(index: number) {
    return this.locator.nth(index);
  }

  public async toBeVisible(timeout?: number) {
    await test.expect(this.locator).toBeVisible({ timeout: timeout ?? 5000 });
  }

  public async toBeHidden(timeout?: number) {
    await test.expect(this.locator).toBeHidden({ timeout: timeout ?? 5000 });
  }

  public async toHaveAttribute(
    name: string,
    expected: string | RegExp,
    options?: { timeout?: number },
  ) {
    await test
      .expect(this.locator)
      .toHaveAttribute(name, expected, options ?? { timeout: 5000 });
  }

  public async toHaveText(
    expected: string | RegExp | (string | RegExp)[],
    options?: { timeout?: number; useInnerText?: boolean },
  ) {
    const mergedOptions = {
      ...{ timeout: 5000 },
      ...options,
    };

    await test.expect(this.locator).toHaveText(expected, mergedOptions);
  }

  public async toHaveCss(
    name: string,
    expected: string,
    options?: { timeout?: number },
  ) {
    await test
      .expect(this.locator)
      .toHaveCSS(name, expected, options ?? { timeout: 5000 });
  }

  public async toContainText(
    expected: string | RegExp | (string | RegExp)[],
    options?: { timeout?: number; useInnerText?: boolean },
  ) {
    await test.expect(this.locator).toContainText(expected, options);
  }

  public async getText() {
    const textContent = await this.locator.textContent();

    if (textContent === null) {
      throw new Error(`text content for ${this.locator} is null`);
    }

    return textContent;
  }

  public async getAttribute(attribute: string) {
    const attributeValue = await this.locator.getAttribute(attribute);
    if (attributeValue === null) {
      throw new Error(`specific attribute for ${this.locator} is null`);
    }
    return attributeValue;
  }

  public async getBoundingBox() {
    const boundingBox = await this.locator.boundingBox();

    if (boundingBox === null) {
      throw new Error(`bounding box for ${this.locator} is null`);
    }

    return boundingBox;
  }
}
