import {ComponentPage, IComponentPageProps} from './component.page';
import {InputPage} from './input.page';

export class FieldPage extends ComponentPage {
    public fieldLabel: ComponentPage;

    public input: InputPage;

    constructor(props: IComponentPageProps) {
        super(props);

        this.fieldLabel = new ComponentPage({
            page: this.page,
            locator: this.locator.locator('label'),
        });

        this.input = new InputPage({
            page: this.page,
            locator: this.locator.locator('input'),
        });
    }
}

