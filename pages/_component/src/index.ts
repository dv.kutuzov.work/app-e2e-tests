export { ComponentPage, IComponentPageProps } from './component.page';

export { InputPage } from './input.page';

export { FieldPage } from './field.page';

export { CheckboxPage } from './checkbox.page';
