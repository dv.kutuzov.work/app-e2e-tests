import { ComponentPage, IComponentPageProps } from '@e2e-tests/components';

export class VideoPlayerPage extends ComponentPage {
  public video: ComponentPage;
  
  public volumeToggle: ComponentPage;

  public videoPurchaseSection: ComponentPage;

  constructor(props: IComponentPageProps) {
    super(props);

    this.video = new ComponentPage({
      page: this.page,
      locator: this.locator.locator('video'),
    });

    this.volumeToggle = new ComponentPage({
      page: this.page,
      locator: this.locator.locator('[class="video-purchase__volume-toggle"]'),
    });

    this.videoPurchaseSection = new ComponentPage({
      page: this.page,
      locator: this.locator.locator('[class="video-purchase__menu"]'),
    });
  }

  public static init(props: Omit<IComponentPageProps, 'locator'>) {
    return new VideoPlayerPage({
      ...props,
      locator: props.page.locator('[data-el="VideoPurchaseSimple"]'),
    });
  }

  public async checkMuted() {
    await this.video.toHaveAttribute('muted', '');
  }

  public async checkAutoplay() {
    await this.video.toHaveAttribute('autoplay', '');
  }
}
