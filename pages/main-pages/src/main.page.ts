import { ComponentPage, IComponentPageProps } from '@e2e-tests/components';
import { ContentSliderPage } from './form/contentSlider.page';

export class MainPage extends ComponentPage {
  public headerBannerSlider: ComponentPage;

  public byNewCreatorsSlider: ContentSliderPage;

  constructor(props: IComponentPageProps) {
    super(props);

    this.headerBannerSlider = new ComponentPage({
      page: this.page,
      locator: this.locator.locator('h2'),
    });

    this.byNewCreatorsSlider = new ContentSliderPage({
      page: this.page,
      locator: this.locator.locator('[data-el="ContentSlider"]').nth(0),
    });
  }

  public static init(props: Omit<IComponentPageProps, 'locator'>) {
    return new MainPage({
      ...props,
      locator: props.page.locator('[data-el="PageContentWrapper"]'),
    });
  }
}
