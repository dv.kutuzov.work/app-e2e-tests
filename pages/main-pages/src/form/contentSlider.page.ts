import { ComponentPage, IComponentPageProps } from '@e2e-tests/components';
import { SliderVideoPage } from './control/sliderVideo.page';

export class ContentSliderPage extends ComponentPage {
  private sliderVideo: SliderVideoPage;

  constructor(props: IComponentPageProps) {
    super(props);

    this.sliderVideo = new SliderVideoPage({
      page: this.page,
      locator: this.locator.locator('[data-el="Thumb"]')
    });
  }
  
  public async clickRandomVideo() {
    const randomNth = Math.floor(Math.random() * 5)
    
    await this.sliderVideo.nth(randomNth).click();
  }
}
