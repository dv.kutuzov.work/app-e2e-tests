import { ComponentPage, IComponentPageProps } from '@e2e-tests/components';

export class SliderVideoPage extends ComponentPage {
  public videoMain: ComponentPage;

  public videoLink: ComponentPage;

  private videoTitle: ComponentPage;

  constructor(props: IComponentPageProps) {
    super(props);

    this.videoMain = new ComponentPage({
      page: this.page,
      locator: this.locator.locator('[class="thumb__main"]'),
    });

    this.videoTitle = new ComponentPage({
      page: this.page,
      locator: this.locator.locator('[class="thumb__title"]'),
    });

    this.videoTitle = new ComponentPage({
      page: this.page,
      locator: this.locator.locator('[class="thumb__main"]').locator('a'),
    });
  }

  public async getVideoLink() {
    await this.videoTitle.getAttribute('href');
  }
}
