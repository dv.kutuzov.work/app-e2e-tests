import { faker } from '@faker-js/faker';

export interface IUserParams {
  loginName: string;
  email: string;
  password: string;
  firstName: string;
  lastName: string;
  phone: string;
}

export class User {
  public loginName: string;

  public email: string;

  public password: string;

  public firstName: string;

  public lastName: string;

  public phone: string;

  public constructor(params: IUserParams) {
    this.loginName = params.loginName;
    this.email = params.email;
    this.password = params.password;
    this.firstName = params.firstName;
    this.lastName = params.lastName;
    this.phone = params.phone;
  }

  public static init(): User {
    const firstName = faker.person.firstName()
    const lastName = faker.person.lastName()
    const loginName =
      `${firstName}.${lastName}-${Date.now()
        .toString()
        .substr(3)}`
        .toLowerCase()
        .replace("'", '');
    const email = `${loginName}@test.com`;
    const phone = faker.phone.number('501-###-###');

    return new User({
      loginName,
      email,
      password: email,
      firstName,
      lastName,
      phone,
    });
  }
}
