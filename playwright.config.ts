import { defineConfig, devices } from '@playwright/test';

export default defineConfig({
  testDir: './e2e-tests/',
  testMatch: '**/*.spec.ts',
  timeout: 120000,
  workers: 1,
  reportSlowTests: null,
  expect: {
    timeout: 5000
  },
  reporter: 'html',
  use: {
    ...devices['Desktop Chrome'],
    trace: 'on',
    headless: false,
    ignoreHTTPSErrors: true,
    video: 'off',
    screenshot: 'on',
    viewport: {width: 1280, height: 720},
  },
});
