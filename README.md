## Installation

Install node modules and playwright browsers:

```sh
npm install
npx playwright install
```

## Running tests

Run all tests:

```sh
npx playwright test -c playwright.config.ts
```
